FROM python:3.8-slim

WORKDIR /app

COPY . /app

RUN mkdir /app/jobs

RUN python3 -m pip install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

CMD [ "python3", "-u", "app.py"]
