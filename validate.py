import cgi
import re
from urllib.parse import urlparse

import requests

# branch Namen sollen aus Wörtern getrennt mit Bindestrichen bestehen
BRANCH_PATTERN = re.compile(r"(\w+-)*\w+")

# format ist https://t.me/NAME oder https://t.me/joinchat/INVITE_ID
URL_PATTERN = re.compile(r"(http(s)?://)?t.me(?:/joinchat)?/([^/]+)")


class BranchNameError(Exception):
    def __init__(self, message):
        self.message = message


class TelegramUrlError(Exception):
    def __init__(self, message):
        self.message = message


class UrlFormatError(TelegramUrlError): pass


class GroupUrlExists(TelegramUrlError):
    def __init__(self):
        self.message = "Diese Gruppe kenne ich berreits, besuche doch einmal die Website. https://dachverband.gitlab.io/dach-url-list/"


def sanitize_branch_name(branch_name):
    sanitized_name = branch_name.replace(" ", "-").replace("_", "-")
    if BRANCH_PATTERN.fullmatch(sanitized_name) is None:
        raise BranchNameError(f"Ungültige Zeichen im Branch Name {sanitized_name}")
    return sanitized_name


def validate_tg_url(url):
    is_url_already_in_list(url)
    url_match = URL_PATTERN.fullmatch(url)
    if url_match is None:
        raise UrlFormatError("Dein Link ist leider kein valider t.me Gruppenlink.")
    if url_match.groups()[0] is None:
        url = "https://" + url

    parse_result = urlparse(url)
    # extra check, ob nicht irgendwas komisches übergeben wurde, was nicht als url erkannt wird
    if parse_result.scheme not in ["http", "https"] or parse_result.netloc is None or parse_result.netloc == '':
        raise UrlFormatError("Dein Link ist leider keine valide URL.")

    return url


def sanitize_group_name(name):
    # so kann der Name keinen komischen HTML Kram machen
    return cgi.escape(name)


def is_url_already_in_list(url):
    x = requests.get("https://dachverband.gitlab.io/dach-url-list/api.json").json()
    url_list = []
    for g in x["groups"]:
        url_list.append(g["url"])
    if any(url in word for word in url_list):
        raise GroupUrlExists()
