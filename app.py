import json
import logging
import os
import time

import requests
from gitlab import Gitlab
from pyrogram import Client, filters
from pyromod import listen

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

from validate import (BranchNameError, UrlFormatError, sanitize_branch_name,
                      validate_tg_url, GroupUrlExists)

app = Client(
    os.environ['BOT_NAME'],
    api_id=os.environ['API_ID'],
    api_hash=os.environ['API_HASH'],
    bot_token=os.environ['BOT_TOKEN']
)

@app.on_message(filters.command("status", "/"))
async def status(client, msg):
    await msg.reply_text(f"{os.environ['BOT_NAME']} Online")


@app.on_message(filters.command("start", "/"))
async def start(client, msg):
    link = await app.ask(msg.chat.id, 'Wie lautet der Link zu deiner Gruppe? - Beispiel: https://t.me/group_name')
    try:
        group_url = validate_tg_url(link.text)
    except (UrlFormatError, GroupUrlExists) as e:
        await app.send_message(msg.from_user.id, str(e.message))
        return

    name = await app.ask(msg.chat.id, 'Wie ist der Name deiner Gruppe?')
    tags = await app.ask(msg.chat.id,
                         'Möchtest du Tags zu deiner Gruppe hinzufügen? - tag1, tag2, .. -- Falls du keine Tags nutzen möchtest, Sende mir ".none"')
    if tags.text == ".none":
        taglist = ""
    else:
        taglist = tags.text.split(",")

    new_group = {"name": name.text, "url": group_url, "tags": taglist}

    with open(f"jobs/{new_group['name']}.json", 'w+') as file_name:
        json.dump(new_group, file_name, indent=4)

    await client.send_message(msg.chat.id,
                              "Danke, deine Gruppe wird nach der Bestätigung durch einen Admin auf der Website zu sehen sein. https://dachverband.gitlab.io/dach-url-list/")

    print(f"New group add requested - {new_group}")
    logging.info(f"New group add requested by {msg.chat.id} - {new_group}")

    gl = Gitlab("https://gitlab.com", private_token=str(os.environ['GITLAB_TOKEN']))
    p = gl.projects.get("dachverband/dach-url-list")
    try:
        branch_name = sanitize_branch_name(f"add-group-{new_group['name']}")
    except BranchNameError as e:
        await msg.reply_text(e.message)
        return

    for file_name in os.listdir("jobs"):
        branch = p.branches.create({'branch': branch_name, 'ref': 'master'})
        data = {
            'branch': branch.name,
            'commit_message': f'add group from Bot - {new_group["name"]}',
            'actions': [
                {
                    'action': 'create',
                    'file_path': f'groups/{file_name}',
                    'content': json.dumps(new_group),
                }]}
        p.commits.create(data)
        logging.info("Sleep before Merge ..")
        time.sleep(2)
        mr = p.mergerequests.create({'source_branch': branch.name, 'target_branch': 'master',
                                     'title': f'New Group from TG Bot - {new_group["name"]}',
                                     'description': f"Erstellt über Telegram Bot von  {msg.chat.username}",
                                     'remove_source_branch': True, })
        logging.info(f"MR {mr.id} created")
        os.remove(f"jobs/{file_name}")
        app.send_message(os.environ['BOT_LOG'], f"MR {mr.id} created - {new_group}")

if __name__ == "__main__":
    if not os.path.isdir('jobs'):
        os.mkdir('jobs')

    logging.info("Bot is Online")
    app.run()
