# DACH Bot

Telegram bot for adding new groups to the DACH Group List

[@dach_website_bot](https://t.me/dach_website_bot)

## Run

Create a `config.ini` file with the following content:

```
[pyrogram]
api_id = 
api_hash = 

[bot]
bot_name = ""
bot_token = 

[gitlab]
token = 
original_project = 
```

Run the container:

`docker run -d --name dachbot -v /path/to/config.ini:/app/config.ini registry.gitlab.com/fuchsr/telegram-bot-dach-addgrouptowebsite:latest`


## libs
[Pyrogram](https://docs.pyrogram.org/)
[Python Gitlab](https://python-gitlab.readthedocs.io/en/stable/)
